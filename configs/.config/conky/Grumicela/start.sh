#!/bin/bash

killall conky
sleep 2s
		
conky -c $HOME/.config/conky/Grumicela/Celaeno.conf &> /dev/null &
conky -c $HOME/.config/conky/Grumicela/Grumium2.conf &> /dev/null &

exit
